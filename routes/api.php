<?php

use App\Http\Controllers\API\PatientApiController;
use App\Http\Controllers\API\ProductApiController;
use App\Http\Controllers\API\ProductCategoryApiController;
use App\Http\Controllers\API\SupplierApiController;
use App\Http\Controllers\API\UserApiController;
use App\Http\Controllers\API\UserGroupApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// User Api

Route::get('/users', [UserApiController::class, 'index'])->name('users.index');
Route::get('/users/{id}', [UserApiController::class, 'show'])->name('users.get');
Route::post('/users', [UserApiController::class, 'store'])->name('user.create');
Route::delete('/users/{id}', [UserApiController::class, 'destroy'])->name('users.destroy');

// User Group Api
Route::get('/userGroups', [UserGroupApiController::class, 'index'])->name('userGroup.index');
Route::post('/userGroups', [UserGroupApiController::class,'store'])->name('userGroup.create');

// Product Api 
Route::get('/products', [ProductApiController::class , 'index'])->name('products.index');
Route::post('/products', [ProductApiController::class , 'store'])->name('products.store');
Route::get('/products/{id}', [ProductApiController::class,'show'])->name('products.show');

// ProductCategory

Route::get('/product/categories', [ProductCategoryApiController::class, 'index'])->name('ProductCategories.index');


// Supplier

Route::get('/suppliers', [SupplierApiController::class, 'index'])->name('suppliers.index');


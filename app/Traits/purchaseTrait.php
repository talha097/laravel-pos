<?php
namespace App\Traits;
use App\Models\Purchase;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


trait PurchaseTrait{
    private function getRefenceID($type){
        $table = $type == 'PUR' ? 'purchases' : 'selling_info';
        $transactionID = DB::table($table)->select('id')->latest('id')->first();
        $transactionID =  $transactionID ?  $transactionID->id + 1 : 1;
        return $type . '-' . $transactionID . str_replace(':','',Carbon::now()->toTimeString());
    }

    private function getCurrentStock($id){
        $currentStock = DB::table('products')->select('stock')->where('id', '=', $id)->first();
        return $currentStock->stock;
    }
}
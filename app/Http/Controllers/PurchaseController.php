<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use App\Models\Purchase;
use App\Models\PurchaseItem;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\PurchaseTrait;


class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('purchase.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all('id','name');
        $suppliers = Supplier::all('id','name');
        return view('purchase.create', compact('products','suppliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request);
        $invoiceId = $this->getRefenceID('PUR');

        $length = count($request->id);
        $validator = Validator::make($request->all(), [
            'id'            => 'required|array|size:'. $length,
            'supplier_id'   => 'required',
            'buyPrice'      => 'required|array|size:'. $length,
            'sellPrice'     => 'required|array|size:'. $length,
            'quantity'      => 'required|array|size:'. $length,
            'totalAmount'   => 'required|array|size:'. $length,
            'totalQuantity' => 'required',
            'subTotal'      => 'required',
            'description'   => 'sometimes'
        ]);
        if($validator->passes()){

            // storing purchase Info
            $purchase = new Purchase;
            $purchase->invoice_id = $invoiceId;
            $purchase->supplier_id = $request->supplier_id;
            $purchase->total_items = $request->totalQuantity;
            $purchase->total_amount = $request->subTotal;
            $purchase->description  = $request->description;
            $purchase->created_by  = Auth::user()->id;

            // Storing purchase items
            for( $i=1; $i<=$length; $i++ )
            {   
                $purchaseItems = new PurchaseItem;
                $purchaseItems->invoice_id = $invoiceId;
                $purchaseItems->product_id = $request->id;
                $purchaseItems->buy_price  = $request->buyPrice;
                $purchaseItems->sell_price = $request->sellPrice;
                $purchaseItems->item_quantity = $request->quantity;
                $purchaseItems->stock = $request->quantity;
                $purchaseItems->surrentStock = $this->getCurrentStock($request->id);

            }
        } else{
            dd("failed");
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function show(Purchase $purchase)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        //
    }
}

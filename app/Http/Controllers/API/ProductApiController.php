<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Session\SessionManager;
use Illuminate\Support\Facades\Redirect;
use Prophecy\Promise\ThrowPromise;

class ProductApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $product = DB::table('products')
            ->join('product_categories', 'products.category_id', '=', 'product_categories.id')
            ->select('products.id','products.name','products.category_id','product_categories.name as categoryName')
            ->get();
            return response()->json([
                'message' => 'Product List',
                'code' => 200,
                'data' => $product
            ]);
        }catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        DB::beginTransaction();
        $validated = $request->validate([
            'name' => 'required',
            'p_code' => 'required|integer|unique:products,p_code',
            'category_id' => 'required',
        ]);
        try{
            if($validated){
                $product = new Product;
                $product->p_code = $request->p_code;
                $product->name  = $request->name;
                $product->image = $request->image;
                $product->category_id = $request->category_id;
                $product->description = $request->description;
                $product->save();
                DB::commit();

                return Redirect::to("/product")->withSuccess('Product has been added!');
            } 
        }  catch(Exception $e)
        {
            DB::rollBack();
            return response()->json([
                'code' => 202,
                'message' => 'product not added',
                'error' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $product = Product::select('id', 'name', 'p_code','category_id')->find($id);
            return response()->json([
                'message' => 'Got product info',
                'code' => 200,
                'data' => $product,
            ]);
        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'data' => $product,
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

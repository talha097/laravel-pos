<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('supplier.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('supplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        $validated = $request->validate([
            'name'         => 'required|string',
            'email'        => 'sometimes',
            'address'      => 'required',
            'mobile'       => 'required',
            'city'         =>'required',  
            'state'        =>'required', 
            'country'      =>'required|string',
            'description'  =>'sometimes'  
        ]);
        try{
            if($validated){
                $supplier = new Supplier;
                $supplier->name        = $request->name;
                $supplier->email       = $request->email;
                $supplier->address     = $request->address;
                $supplier->mobile      = $request->mobile;
                $supplier->city        = $request->city;
                $supplier->state       = $request->state;
                $supplier->country     = $request->country;
                $supplier->description = $request->description;
                $supplier->save();
                DB::commit();

                return Redirect::to("/supplier")->withSuccess('supplier has been added!');
            } 
            else
            {
                return redirect::to('/supplier/create')->withErrors($validated)->withInput();
            }
        }  catch(Exception $e)
        {
            DB::rollBack();
            throw new Exception($e->getMessage(),$e->getCode());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit(Supplier $supplier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Supplier $supplier)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier)
    {
        //
    }
}

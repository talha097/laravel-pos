@extends('layouts.app')

@section('content')
<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="/purchase">purchases</a></li>
            <li class="breadcrumb-item active"><a href="/purchase/create">purchase Create</a></li>
        </ol>
    </div>
</div>

<div class="d-flex justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
              <h5 class="card-title">Add Purchase</h5>
    
              <!-- Multi Columns Form -->
              <form class="row g-3" action="{{ route('purchase.store') }}" method="POST">
                @csrf

                <div class="col-md-6">
                  <label for="inputName5" class="form-label">supplier</label>
                  <select name="supplier_id" id="supplier_id" class="form-select">
                      <option value="">Select supplier</option>
                      @foreach ($suppliers as $key => $supplier)
                          <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                      @endforeach
                  </select>
                  @error('supplier_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>

                <div class="col-md-6">
                    <label for="inputName5" class="form-label">Products</label>
                    <select name="products" id="products" class="form-select">
                        <option value="">Select Product</option>
                        @foreach ($products as $key => $product)
                            <option value="{{ $product->id }}">{{ $product->name }}</option>
                        @endforeach
                    </select>
                    @error('products')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="col-md-12">
                    <table class="table">
                        <thead>
                          <tr class="text-center">
                            <th scope="col">#</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Buy Price</th>
                            <th scope="col">Sell Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Total Amount</th>
                            <th><i class="bi bi-trash"></i></th>
                          </tr>
                        </thead>
                        <tbody id="product-detail">

                        </tbody>
                      </table>
                </div>
                <div class="col-md-12 text-center">
                  <table class="table">
                        <thead>
                          <tr>
                            <th>Total Quantity</th>
                            <th>Sub Total</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <input type="hidden" name="totalQuantity" id="_totalQuantity">
                            <input type="hidden" name="subTotal" id="_subTotal">
                            <td id="totalQuantity"></td>
                            <td id="subTotal"></td>
                          </tr>
                        </tbody>
                  </table>
                </div>

                <div class="col-md-8">
                  <label for="inputAddress5" class="form-label">description</label>
                  <textarea type="text" class="form-control" id="description" name="description"></textarea>
                </div>
                
                <div class="text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="reset" class="btn btn-secondary">Reset</button>
                </div>
              </form><!-- End Multi Columns Form -->
    
            </div>
          </div>
    </div>
</div>
@endsection

@push('js')
<script src="{{ asset("js/purchase.js") }}"></script>
@endpush

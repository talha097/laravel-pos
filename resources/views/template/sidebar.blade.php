 <!-- ======= Sidebar ======= -->
 <aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

      <li class="nav-item">
        <a class="nav-link " href="/home">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#product-nav" data-bs-toggle="collapse" href="/product">
          <i class="bi bi-menu-button-wide"></i><span>Products</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="product-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="/product">
              <i class="bi bi-circle"></i><span>Product</span>
            </a>
          </li>
          <li>
            <a href="/product/category">
              <i class="bi bi-circle"></i><span>Product Category</span>
            </a>
          </li>
        </ul>
      </li>
      
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#supplier-nav" data-bs-toggle="collapse" href="/supplier">
          <i class="bi bi-menu-button-wide"></i><span>suppliers</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="supplier-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="/supplier">
              <i class="bi bi-circle"></i><span>supplier</span>
            </a>
          </li>
        </ul>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#purchase-nav" data-bs-toggle="collapse" href="/purchase">
          <i class="bi bi-menu-button-wide"></i><span>purchases</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="purchase-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="/purchase">
              <i class="bi bi-circle"></i><span>purchase</span>
            </a>
          </li>
        </ul>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#user-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-menu-button-wide"></i><span>User</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="user-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="/user">
              <i class="bi bi-circle"></i><span>users</span>
            </a>
          </li>
          <li>
            <a href="/usergroup">
              <i class="bi bi-circle"></i><span>user group</span>
            </a>
          </li>
        </ul>
      </li><!-- End Components Nav -->
    </ul>

  </aside><!-- End Sidebar-->
@extends('layouts.app')

@section('content')
<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="/product/category">ProductCategory</a></li>
            <li class="breadcrumb-item active"><a href="/product/category/create">CategoryCreate</a></li>
        </ol>
    </div>
</div>

<div class="d-flex justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
              <h5 class="card-title">Add Category</h5>
    
              <!-- Multi Columns Form -->
              <form class="row g-3" action="{{ route('productCategory.store') }}" method="POST">
                @csrf
                <div class="col-md-12">
                  <label for="inputName5" class="form-label">Category Name</label>
                  <input type="text" class="form-control" id="name" name="name">
                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-md-6">
                  <label for="inputEmail5" class="form-label">Parent category</label>
                    <select name="parent_category" id="parent_category" class="form-select">
                        <option value="">Select Parent Category</option>
                        @foreach ($productCategories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                    @error('parent_category')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-12">
                  <label for="inputAddress5" class="form-label">description</label>
                  <textarea type="text" class="form-control" id="description" name="description"></textarea>
                </div>
                <div class="text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="reset" class="btn btn-secondary">Reset</button>
                </div>
              </form><!-- End Multi Columns Form -->
    
            </div>
          </div>
    </div>
</div>

@endsection

@push('js')
<script src="{{ asset("js/product.js") }}"></script>
@endpush

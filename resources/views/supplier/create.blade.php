@extends('layouts.app')

@section('content')
<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="/supplier">suppliers</a></li>
            <li class="breadcrumb-item active"><a href="/supplier/create">suppliers Create</a></li>
        </ol>
    </div>
</div>

<div class="d-flex justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
              <h5 class="card-title">Add Supplier</h5>
    
              <!-- Multi Columns Form -->
              <form class="row g-3" action="{{ route('supplier.store') }}" method="POST">
                @csrf

                <div class="col-md-12">
                  <label for="inputName5" class="form-label">Name</label>
                  <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Enter Supplier Name" >
                  @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>

                <div class="col-md-12">
                    <label for="inputName5" class="form-label">Email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                    @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="col-md-12">
                    <label for="inputName5" class="form-label">Address</label>
                    <input type="text" class="form-control @error('address') is-invalid @enderror" id="address" name="address" placeholder="Enter Address" >
                    @error('address')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="col-md-12">
                    <label for="inputName5" class="form-label">Mobile</label>
                    <input type="tel" class="form-control @error('mobile') is-invalid @enderror" id="mobile" name="mobile" placeholder="Enter mobile Number" >
                    @error('mobile')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="col-md-12">
                    <label for="inputName5" class="form-label">city</label>
                    <input type="text" class="form-control @error('city') is-invalid @enderror" id="city" name="city" placeholder="Enter City" >
                    @error('city')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="col-md-12">
                    <label for="inputName5" class="form-label">state</label>
                    <input type="text" class="form-control @error('state') is-invalid @enderror" id="state" name="state" placeholder="Enter state" >
                    @error('state')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="col-md-12">
                    <label for="inputName5" class="form-label">country</label>
                    <input type="text" class="form-control @error('country') is-invalid @enderror" id="country" name="country" value="Pakistan" placeholder="Enter country" >
                    @error('country')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="col-12">
                  <label for="inputAddress5" class="form-label">description</label>
                  <textarea type="text" class="form-control" id="description" name="description"></textarea>
                </div>
                
                <div class="text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="reset" class="btn btn-secondary">Reset</button>
                </div>
              </form><!-- End Multi Columns Form -->
    
            </div>
          </div>
    </div>
</div>

@endsection

@push('js')
<script src="{{ asset("js/product.js") }}"></script>
@endpush

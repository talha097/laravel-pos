@extends('layouts.app')

@section('content')
<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="/supplier">Supplier</a></li>
        </ol>
    </div>
</div>

@if(Session::has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{Session::get('success')}}</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">supplier Table</h5>
              <a class="btn btn-primary mb-4" href="{{ route('supplier.create') }}" role="button">Add Supplier</a>
              <!-- Table with stripped rows -->
              <table id="supplier-table" class="table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>city</th>
                        <th>view</th>
                        <th>edit</th>
                        <th>delete</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>city</th>
                        <th>view</th>
                        <th>edit</th>
                        <th>delete</th>
                    </tr>
                </tfoot>
              </table>
              <!-- End Table with stripped rows -->

            </div>
          </div>

        </div>
      </div>
</div>
@endsection


@push('js')
<script src="{{ asset("js/supplier.js") }}"></script>
@endpush
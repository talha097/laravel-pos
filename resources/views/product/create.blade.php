@extends('layouts.app')

@section('content')
<div class="row page-titles mx-0">
    <div class="col p-md-0">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="/product">Products</a></li>
            <li class="breadcrumb-item active"><a href="/product/create">Products Create</a></li>
        </ol>
    </div>
</div>

<div class="d-flex justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
              <h5 class="card-title">Add Product</h5>
    
              <!-- Multi Columns Form -->
              <form class="row g-3" action="{{ route('products.store') }}" method="POST">
                @csrf
                <div class="mb-3">
                    <label for="formFile" class="form-label">Upload Image</label>
                    <input class="form-control" type="file" id="image" name="image">
                    
                </div>
                <div class="col-md-12">
                  <label for="inputName5" class="form-label">Name</label>
                  <input type="text" class="form-control" id="name" name="name" required>
                    @error('name')
                        <div class="invalid-tooltip">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-md-12">
                    <label for="inputName5" class="form-label">Product Code</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <button class="btn btn-outline-secondary" type="button" id="random_num"><i class="bi bi-shuffle"></i></button>
                        </div>
                        <input type="text" class="form-control" id="p_code" name="p_code" required>
                        @error('p_code')
                        <div class="invalid-tooltip">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                  <label for="inputEmail5" class="form-label">category</label>
                    <select name="category_id" id="category_id" class="form-select" required>
                        <option value="">Select Category</option>
                        @foreach ($productCategories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                    @error('category_id')
                        <div class="invalid-tooltip">{{ $message }}</div>
                    @enderror
                </div>
                <div class="col-12">
                  <label for="inputAddress5" class="form-label">description</label>
                  <textarea type="text" class="form-control" id="description" name="description"></textarea>
                </div>
                <div class="text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="reset" class="btn btn-secondary">Reset</button>
                </div>
              </form><!-- End Multi Columns Form -->
    
            </div>
          </div>
    </div>
</div>

@endsection

@push('js')
<script src="{{ asset("js/product.js") }}"></script>
@endpush

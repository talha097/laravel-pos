"use strict";
// Variables
let id, quantity, buyPrice, total;
let subTotal;

// Getting products
$("#products").on("change", function () {
    let productId = $("#products").val();
    if (productId) {
        $.ajax({
            url: "http://pos.test/api/products" + "/" + productId,
            beforeSend: function (xhr) {
                xhr.overrideMimeType("text/plain; charset=x-user-defined");
            },
        }).done(function (data) {
            let obj = JSON.parse(data);
            let i = 1;
            let html =
                '<tr id="' +
                obj.data.id +
                '" class="' +
                obj.data.id +
                ' text-center">';
            html += "<th scope='row'>" + obj.data.id + "</th>";
            html +=
                '<input name="id[' +
                obj.data.id +
                ']" type="hidden" value="' +
                obj.data.id +
                '">';
            html += "<td>" + obj.data.name + "</td>";
            html += "<td>";
            html +=
                "<input type='number' min=0 class='form-control cost' name = 'buyPrice[" +
                obj.data.id +
                "]' id='buy_price_" +
                obj.data.id +
                "'>";
            html += "</td>";
            html += "<td>";
            html +=
                "<input type='number' min=0 class='form-control' name = 'sellPrice[" +
                obj.data.id +
                "]' id='sell_price'>";
            html += "</td>";
            html += "<td>";
            html +=
                "<input type='number' min=0 class='form-control quantity' name = 'quantity[" +
                obj.data.id +
                "]' id='quantity_" +
                obj.data.id +
                "'>";
            html += "</td>";
            html +=
                "<input type='hidden' class='form-control' name='totalAmount[" +
                obj.data.id +
                "]' id = 'total_amount" +
                obj.data.id +
                "'>";
            html += "<td>";
            html +=
                "<input type='number' class='form-control' name='total_amount[" +
                obj.data.id +
                "]' id = 'total_amount_" +
                obj.data.id +
                "' disabled>";
            html += "</td>";
            html += "<td id ='removeProduct'>";
            html += "<i class='bi bi-x-lg'></i>";
            html += "</td>";
            html += "</tr>";

            if ($("#" + obj.data.id).length) {
                alert("Product Alredy selected");
            } else {
                $("#product-detail").append(html);
            }
        });
    }
});

$(document).on("change", ".quantity, .cost", function () {
    id = $(this).parent().parent().attr("id");
    calculate(id);
});

$("#product-detail").on("click", "#removeProduct", function () {
    $(this).closest("tr").remove();
    id = $(this).parent().parent().attr("id");
    calculate(id);
});

// Calcualtion
function calculate(id) {
    buyPrice = $("[name='buyPrice[" + id + "]']").val();
    quantity = $("[name='quantity[" + id + "]']").val();
    // Calculating Total price of single product
    total = buyPrice * quantity;
    $("#total_amount_" + id).val(total);
    $("#total_amount" + id).val(total);

    // Getting All Prices in array
    let allPrices = $(".cost")
        .map((i, e) => e.value)
        .get();
    // Getting All Quantities in array
    let allQts = $(".quantity")
        .map((i, e) => e.value)
        .get();
    // calculating sub Total
    subTotal = allQts.reduce(function (r, a, i) {
        return r + a * allPrices[i];
    }, 0);
    // Calculating total quantity
    let totalQuantity = allQts.reduce(function (r, a) {
        return r + Number(a);
    }, 0);

    $("#subTotal").text(subTotal);
    $("#_subTotal").val(subTotal);
    $("#totalQuantity").text(totalQuantity);
    $("#_totalQuantity").val(totalQuantity);
}

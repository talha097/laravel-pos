"use strict";

jQuery(function () {
    $("#product-category-table").DataTable({
        ajax: "/api/product/categories",
        method: "GET",
        columns: [
            { data: "id" },
            { data: "name" },
            { data: "parentCategoryName" },
            {
                data: null,
                render: function (data, type, row) {
                    return (
                        '<a id="editBtn" href="product/' +
                        data.id +
                        '/edit" class="btn btn-success"><i class="bi bi-pen"></i></a>'
                    );
                },
            },
            {
                data: null,
                render: function (data, type, row) {
                    return (
                        '<button id="deleteBtn" class="btn btn-danger" onclick="showUserDelete(' +
                        data.id +
                        ')"  value="' +
                        data.id +
                        '"><i class="bi bi-trash"></i></button>'
                    );
                },
            },
        ],
        columnDefs: [
            { responsivePriority: 1, targets: 1 },
            { className: "text-center", targets: [0, 1, 2, 3] },
        ],
    });
});
